package eu.andret.spring.example.entity;

import java.util.ArrayList;
import java.util.List;

public record Game(int id, List<Player> players) {
	public Game(final int id) {
		this(id, new ArrayList<>());
	}
}
