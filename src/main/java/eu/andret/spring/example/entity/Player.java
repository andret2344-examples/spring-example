package eu.andret.spring.example.entity;

import java.util.Objects;

public class Player {
	private final int id;
	private final String name;
	private int level;

	public Player(final int id, final String name) {
		this.id = id;
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public int getLevel() {
		return level;
	}

	public void setLevel(final int level) {
		this.level = level;
	}

	@Override
	public String toString() {
		return "Player{" +
				"id=" + id +
				", name='" + name + '\'' +
				", level=" + level +
				'}';
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final Player player = (Player) o;
		return id == player.id && level == player.level && Objects.equals(name, player.name);
	}

	@Override
	public int hashCode() {
		return Objects.hash(id, name, level);
	}
}
