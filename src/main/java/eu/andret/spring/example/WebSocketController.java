package eu.andret.spring.example;

import org.springframework.messaging.handler.annotation.MessageExceptionHandler;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;

@Controller
public class WebSocketController {
	@MessageMapping("/chat")
	@SendTo("/topic/messages")
	public String send() {
		System.out.println("XYZ");
		return "Hello from game";
	}

	@MessageExceptionHandler
	@SendToUser("/queue/errors")
	public String handleException(final Throwable exception) {
		return exception.getMessage();
	}
}
